import io

from setuptools import find_packages, setup

with io.open('README.md', 'rt') as f:
    readme = f.read()

setup(
    name='betterfordi',
    version='1.0.0',
    url='http://gitlab.com/novabrahm/betterfordi',
    license='Public Domain',
    maintainer='James Brahm',
    maintainer_email='james@jamesbrahm.us',
    description='Description goes here',
    long_description=readme,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
    extras_require={
        'test': [
            'pytest',
            'coverage',
        ],
    },
)

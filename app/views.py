from flask import render_template, g, Blueprint

import flask_login
import flask_security
import flask_wtf

web_view = Blueprint('my_view', __name__)

@web_view.route('/', methods=('GET', 'POST'))
@flask_security.login_required
def index():
    return render_template('index.html')


